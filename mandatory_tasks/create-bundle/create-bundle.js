const path = require('path')
const fs = require('fs')

const conf = require('bim-gulp/utils/Config')
const prompts = require('prompts')

/**
 * Créer un bundle
 */
function createBundle(done) {
  prompts([
    {
      type: 'text',
      name: 'bundle',
      message: 'Quel est le nom de votre bundle ?',
    }
  ]).then(result => doCreateBundle(result, done))
}


function doCreateBundle(data, done) {
  const bundleRepo = conf.get('build').build.src;

  conf.getEnabledTasks()
      .map(task => conf.get(task)[task])
      .filter(item => item && item.src && item.src != bundleRepo)
      .map(item => item.src)
      .map(item => item.split('/').filter(val => val.indexOf('*') < 0)[0])
      .map(item => fs.mkdirSync(path.join(process.cwd(), bundleRepo, data.bundle, item), {recursive: true}));

  done()
}

exports["create-bundle"] = createBundle
module.exports = createBundle;
