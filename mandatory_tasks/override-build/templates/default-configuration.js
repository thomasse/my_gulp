// Default conf
exports.defaultConfiguration = {
    'build': {
        // Répertoire des sources comprenant les bundles
        src: 'bundles/',
        // Répertoire de destination des builds
        dev: '../dev',
        // Répertoire de destination de prod.
        prod: '../dist',
    }
}
