const {series, parallel} = require('gulp')

const {productionMode} = require('bim-gulp/tasks/build/productionMode')
const {clean} = require('bim-gulp/tasks/clean/clean')
const {scripts} = require('bim-gulp/tasks/scripts/scripts')
const {sprites} = require('bim-gulp/tasks/sprites/sprites')
const {fonts} = require('bim-gulp/tasks/fonts/fonts')
const {sass} = require('bim-gulp/tasks/sass/sass')
const {iconfont}= require('bim-gulp/tasks/iconfont/iconfont')
const {copy} = require('bim-gulp/tasks/copy/copy')

// Build en mode dev.
exports.build = series(
    clean,
    copy,

    parallel(
        // Scripts et css sont indépendants
        scripts,
        // En revanche le sass a besoin des icones, des images et des fonts.
        series(
            iconfont,
            fonts,
            sprites,
            sass
        )
    )
);

// Build en mode production.
exports.production = series(productionMode, exports.build)
// Alias
exports.prod = exports.production
exports.beforeWatch = exports.build
