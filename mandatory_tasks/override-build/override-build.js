const {src, series, dest} = require('gulp')
const path = require('path')
const prompts = require('prompts')
const consolidate = require('gulp-consolidate')
const camelCase = require('camelcase');
const rename = require('gulp-rename')

const conf = require('../../utils/Config')
const messenger = require('../../utils/Messenger')


/**
 * Effectue la tache de font dans un bundle précis.
 * @param bundlePath
 * @return {any}
 */
function createTask(done) {

    prompts([
        {
            type: 'text',
            name: 'dir',
            message: 'Dans quel répertoire voulez vous la créer ?',
        }
    ]).then(result => doCreateTask(result, done))
}

function doCreateTask(options, done) {
    options.task = "build"
    options.camelTask = camelCase(options.task)

    series((done) => {
        return src(path.join(__dirname, 'templates', '*.js'))
            .pipe(consolidate('lodash', options))
            .pipe(rename((path) => {
                // On renomme le fichier de tache au couleur de la tache.
                if (path.basename === 'task') {
                    path.basename = options.camelTask
                }
                return path
            }))
            .pipe(dest(path.join(conf.root, options.dir, options.camelTask)))
            .on('end', () => {
                messenger.info("================ IMPORTANT ===========================")
                messenger.info("N'oubliez pas de loader les taches de ce répertoire ")
                messenger.info("Ajoutez la ligne suivante dans le gulpfile : ")
                messenger.info("taskLoader.loadTask('./" + options.dir + "', exports);")
                messenger.info("                                           ")
                messenger.info("Ensuite vous pourrez lancer la commande ")
                messenger.info("./gulp " + options.camelTask)
                messenger.info("======================================================")
            })
    })(done);
}

exports['override-build'] = createTask;
