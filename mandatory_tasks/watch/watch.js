const {series, task} = require('gulp')

const TaskLoader = require('../../utils/TaskLoader')
const Messenger = require('../../utils/Messenger')
const ReloadTool = require('../../utils/ReloadTools')


/**
 * Appelle à toutes les méthodes de watch processes.
 *
 * Pour ajouter une tache appeler lors d'un watch, il faut
 * exporter une méthode au nom de watchProcess
 */
function watcher() {

    // Initialisation du tools de reload automatique : LiveReload ou browsersync.
    ReloadTool.initReloadTool().then(() =>{
        // Appel des watch processes.
        try {
            TaskLoader.watchProcesses.forEach(watchProcess => watchProcess())
        } catch (e) {
            Messenger.error(e)
        }
    })

}

exports.watch = (done) => {
    return series(...Object.values(TaskLoader.beforeWatch), watcher)(done)
}
