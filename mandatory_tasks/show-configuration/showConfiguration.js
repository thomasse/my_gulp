const messenger = require('../../utils/Messenger')
const conf = require('../../utils/Config')

/**
 * Affiche la liste des taches définies.
 * @param done
 */
function showConfiguration(done) {
    messenger.title('Voici la configuration actuelle :')
    messenger.message(JSON.stringify(conf.config, null, 2))
    done()
}

exports['show-configuration'] = showConfiguration
