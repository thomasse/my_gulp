// Default conf
exports.defaultConfiguration = {
    '<%= camelTask %>': {
        // Répertoire des sources
        ext: '*',
        src: '<%= camelTask %>/*',
        watch: '<%= camelTask %>/**/*',
        // Répertoire de destination.
        dest: './',
    }
}
