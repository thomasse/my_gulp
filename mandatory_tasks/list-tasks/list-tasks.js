const { series, src, watch } = require('gulp');

const messenger = require('../../utils/Messenger')
const taskLoader = require('../../utils/TaskLoader');

/**
 * Affiche la liste des taches définies.
 * @param done
 */
function listTasks(done) {
    messenger.title('Voici la liste des taches gulp disponibles :')
    messenger.message(taskLoader.getTasksList())
    done()
}

exports['list-tasks'] = listTasks;
