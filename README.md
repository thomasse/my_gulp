# Gulp Workflow

## Install
1. Installer le package : 
`npm i bim-gulp`

## Taches utiles : 
**Lancer le watch**:   
`./gulp`  
ou `./gulp watch`

**Lancer une tache spécifique**:  
- En mode dev : `./gulp {nom de la tache}`
- En mode prod : `./gulp {nom de la tache} --production`  

**Lancer le build**:   
- En mode dev : `./gulp build`
- En mode prod : `./gulp production` ou `./gulp prod` ou './gulp build --production'  
 
**Lister les taches disponibles**:   
`./gulp list-tasks`  

**Voir la configuration actuelle**:   
`./gulp show-configuration`  

**Créer une tâche custom**:   
`./gulp create-task`  


## Documentation
- [Principe](./doc/principe.md)
- [Créer une tâche custom](./doc/custom-task.md)
