/**
 * Vous pouvez surcharger ici la configuration par défaut de chaque tache.
 */
module.exports = {
<%= JSON.stringify(config, null, 2).split('\n').slice(1,-1).map(function(line){ return '//' + line + '\n' }).join('') %>
};
