/**
 * Load toutes les taches par défaut présente dans bim-gulp/tasks/*.js
 * @type {TaskLoaderClass}
 */
const taskLoader = require('bim-gulp/utils/TaskLoader');

// Load les taches par défaut
taskLoader.loadDefaultTask(exports);

// Définition de la tache par défaut :
exports.default = exports.watch

// Load les taches custom
// taskLoader.loadAllTasks('./custom-tasks', exports);
