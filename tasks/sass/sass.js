const {watch, src, dest} = require('gulp')
const path = require('path')
const gulpif = require('gulp-if')
const sass = require('gulp-sass')(require('sass'))
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const rename = require('gulp-rename')
const sourcemaps = require('gulp-sourcemaps')

const Messenger = require('../../utils/Messenger')
const Reload = require('../../utils/ReloadTools')
const handleErrors = require('../../utils/error')
const conf = require('../../utils/Config')
const sassConfig = conf.get('sass').sass

/**
 * Build le sass.
 */
function buildSass() {
    return src(conf.getSrcPath(sassConfig.src))
        .pipe(gulpif(!conf.isProd(), sourcemaps.init()))
        .pipe(sass())
        .on('error', handleErrors)
        .pipe(gulpif(conf.isProd(), cleanCSS()))
        .pipe(autoprefixer())
        .pipe(rename((path) => {
            // On supprime la référence au répertoire 'scss' inutile en destination
            path.dirname = path.dirname.split('/').slice(0, -1)
            path.dirname.push(sassConfig.dest)
            path.dirname = path.dirname.join('/');
            return path
        }))
        .pipe(gulpif(!conf.isProd(), sourcemaps.write('.')))
        .pipe(dest(conf.getDestPath()));
}

/**
 * Lance les actions au watch des sass.
 */
function watchProcess() {
	watch(conf.getSrcPath(sassConfig.watch))
		.on('change', (file, data) => {
			// On reconstruit le sass.
			buildSass()
				// On indique au tool de reload automatique qu'un changement à eu lieu.
				.pipe(Reload.dispatchChange(file, data))

			// Log.
			Messenger.info('Changed :' + file)
		})
}

exports.watchProcess = watchProcess
exports.sass = buildSass
