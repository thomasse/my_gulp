const del = require('del');

const conf = require('../../utils/Config')

/**
 * Supprime le contenu des reps.
 * @return {*}
 */
function clean() {
    return del(
        conf.getDestPath('./'),
        {force: true}
    );
}

exports.clean = clean;
