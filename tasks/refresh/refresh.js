const {watch, src, dest} = require('gulp')
const path = require('path')

const Messenger = require('../../utils/Messenger')
const Reload = require('../../utils/ReloadTools')
const conf = require('../../utils/Config')
const refreshConfig = conf.get('refresh').refresh

/**
 * Force le reload du navigateur.
 */
function watchProcess() {

    watch(refreshConfig.watch)
        .on('change', (file, data) => {
            Reload.forceRefresh(file, data)
            Messenger.info('Changed :' + file)
        })
}

exports.watchProcess = watchProcess
