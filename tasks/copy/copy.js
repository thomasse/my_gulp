const {watch, src, dest} = require('gulp')
const path = require('path')

const Reload = require('bim-gulp/utils/ReloadTools')
const bundleTools = require('bim-gulp/utils/BundleTools')
const handleErrors = require('bim-gulp/utils/error')
const conf = require('bim-gulp/utils/Config')

/**
 * Copie de fichier
 */
function copy(done) {
  return bundleTools(conf.getSrcPath('*'), done).src(source => processBundle(source))
}

const config = conf.get('copy').copy

/**
 * Processus scopé dans un bundle particulier.
 */
function processBundle(source) {
  // Récupération de la pattern des points d'entrée
  source = path.join(source, config.src + "." + config.ext)

  // Ex:
  return src(source)
    .on('error', handleErrors)
    .pipe(dest(conf.getDestPath(config.dest) + conf.getFileBundleName(source)))
}

function onFileChanged(file, data) {
  // Pour des raisons de perfs, on ne reconstruit que le bundle concerné par le fichier
  processBundle(conf.getFileBundlePath(file))
    // On indique au tool de reload automatique qu'un changement à eu lieu.
    .pipe(Reload.dispatchChange(file, data))
}

/**
 * Lance les actions au watch des sass.
 */
function watchProcess() {
  const watcher = watch(conf.getSrcPath(path.join('*', config.watch + "." + config.ext)))

  watcher.on('change', (file, data) => onFileChanged(file, data))
}

exports.watchProcess = watchProcess
exports["copy"] = copy

/**
 * A décommenter si la tache doit êter exécutée avant le watch.
 */
//exports.beforeWatch = exports["copy"]
