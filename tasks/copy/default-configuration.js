// Default conf
exports.defaultConfiguration = {
    'copy': {
        // Répertoire des sources
        ext: '*',
        src: 'copy/**/*',
        watch: 'copy/**/*',
        // Répertoire de destination.
        dest: './',
    }
}
