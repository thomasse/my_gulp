const { series } = require('gulp');

const config = require('../../utils/Config');

/**
 * Active le mode de production.
 */
function productionMode (done) {
    config.setProdMode()
    done();
}

exports.productionMode = productionMode;
