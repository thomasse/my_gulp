const {series, parallel} = require('gulp')

const {productionMode} = require('./productionMode')
const {clean} = require('../clean/clean')
const {scripts} = require('../scripts/scripts')
const {sprites} = require('../sprites/sprites')
const {fonts} = require('../fonts/fonts')
const {sass} = require('../sass/sass')
const {iconfont} = require('../iconfont/iconfont')
const {movies} = require('../movies/movies')
const {copy} = require('../copy/copy')


// Build en mode dev.
exports.build = series(
    clean,
    copy,

    parallel(
        // Scripts et css sont indépendants
        scripts,
        // En revanche le sass a besoin des icones, des images et des fonts.
        series(
            iconfont,
            movies,
            fonts,
            sprites,
            sass
        )
    )
);

// Build en mode production.
exports.production = series(productionMode, exports.build)
// Alias
exports.prod = exports.production
exports.beforeWatch = exports.build
