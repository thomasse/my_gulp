const path = require('path');
const conf = require('../../../utils/Config');
const scriptsConf = conf.get('scripts').scripts;

module.exports = {
    mode: 'development',
    output: {
        filename: (pathData) => {
            let destination;

            try{
                fileName = pathData.chunk.name + '.js';
                let bundle = ''
                try {
                    // Mode prod.
                    bundle = conf.getFileBundleName(pathData.chunk.entryModule.rootModule.context);
                }
                catch (e){
                    // Mode dev.
                    bundle = conf.getFileBundleName(pathData.chunk.entryModule.context);
                }
                destination = path.join(bundle, scriptsConf.dest ,  fileName);
            }
            catch (e) {
                // Old way :
                // Récupération du fichier d'entrée.
                const resource = pathData.chunk.entryModule.dependencies[0].module.resource
                // Récupération du chemin des sources du bundle/
                const context = path.join(pathData.chunk.entryModule.context, conf.config.build.src)

                // Construction du chemin de destination relatif
                const destinationPathData = resource.split(context)[1].split('/');
                // Suppresion de la notion de répertoire utile dans les src (/js/).
                destinationPathData.splice(1, 1);
                destination = destinationPathData.join('/')
            }
            return destination;
        },
        path: path.resolve('')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /(node_modules|bower_components)/,
            },
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    }
};
