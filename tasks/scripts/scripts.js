const {watch, src, dest} = require('gulp')
const path = require('path')
const pipedWebpack = require('piped-webpack')
// Utilitaire babel et ts.
const babelLoader = require('babel-loader')
const preset = require('@babel/preset-env')
const ts = require('ts-loader')

const Messenger = require('../../utils/Messenger')
const Reload = require('../../utils/ReloadTools')
const handleErrors = require('../../utils/error')
const webpackConfig = require('./config/webpack.config')
const conf = require('../../utils/Config')
const scriptsConfig = conf.get('scripts').scripts

/**
 * Construit les js.
 * @return {*}
 */
function buildJs() {
    return buildJsInContext(conf.getSrcPath('*'));
};

/**
 * Build le js présent dans le rep source du bundle.
 * @param source
 * @return {any}
 */
function buildJsInContext(source) {
    // Récupération de la pattern des points d'entrée
    source = path.join(source, scriptsConfig.src + scriptsConfig.ext)

    if (conf.isProd()) {
        webpackConfig.mode = "production"
    }

  return src(source)
    .pipe(pipedWebpack(webpackConfig))
    .on('error', handleErrors)
    .pipe(dest(conf.getDestPath()))
}

/**
 * Lance les actions au watch des js.
 */
function watchProcess() {
    const watcher = watch(conf.getSrcPath(path.join('*',scriptsConfig.watch+scriptsConfig.ext)))

    watcher.on('change', (file, data) => {
        // Pour des raisons de perfs, on ne reconstruit que le bundle concerné par le fichier
        buildJsInContext(conf.getFileBundlePath(file))
            // On indique au tool de reload automatique qu'un changement à eu lieu.
            .pipe(Reload.dispatchChange(file, data))
    })
}

exports.watchProcess = watchProcess
exports.scripts = buildJs
