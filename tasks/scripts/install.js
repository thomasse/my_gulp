/**
 * à l'insatllation de la tache.
 * @param data
 */
module.exports.onInstall = (data) => {
	const path = require('path')
	const fs = require('fs');
	const messenger = require('../../utils/Messenger')

	messenger.info('Installation du tsconfig.json');
	// Copy du tsconfig.json.
	if (!fs.existsSync(path.join(data.conf.root, 'tsconfig.json'))) {
		fs.copyFileSync(
			path.join(__dirname, 'templates', 'tsconfig.json'),
			path.join(data.conf.root, 'tsconfig.json'))
	}
}
