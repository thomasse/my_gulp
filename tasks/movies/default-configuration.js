// Default conf
exports.defaultConfiguration = {
    'movies': {
        // Répertoire des sources
        ext: '{png,jpg,jpeg}',
        src: 'movies/*/**',
        watch: 'movies/**/*',
        // Répertoire de destination.
        dest: './movies/',
        css: {
            templatePath: 'default',
            dest: 'scss/movies/'
        }
    }
}
