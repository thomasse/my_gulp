const {watch, src, dest, series, parallel} = require('gulp')
const cp = require('child_process')
const path = require('path')
const glob = require('glob')
const spritesmith = require('gulp.spritesmith')
const buffer = require('vinyl-buffer')
const consolidate = require('gulp-consolidate')
const sizeOf = require('image-size')

const Reload = require('bim-gulp/utils/ReloadTools')
const handleErrors = require('bim-gulp/utils/error')
const bundleTools = require('bim-gulp/utils/BundleTools')
const conf = require('bim-gulp/utils/Config')
const Messenger = require('bim-gulp/utils/Messenger')
const config = conf.get('movies').movies


/**
 * Create sprite movies
 */
function movies(done) {
    return bundleTools(conf.getSrcPath('*'), done).src(processBundle)
}

/**
 * Processus scopé dans un bundle particulier.
 */
function processBundle(source, done) {
    // Récupération de la pattern des points d'entrée
    source = path.join(source, config.src + "." + config.ext)

    // Splien list de movies
    const files = glob.sync(source);
    const reps = [];
    files.map(item => {
        // const pat = path.join(path.dirname(item), '*.' + config.ext);
        const pat = path.join(path.dirname(item));
        if (!reps.includes(pat)) {
            reps.push(pat)
            // reps[pat] = false
        }
    })

    const callBackList = reps.map(rep => {
        return () => processRep(rep);
    });

    if (callBackList.length > 0){
        return series(
            parallel(
                ...callBackList
            ),
            () => createScssTemplate(reps)
        )(done);
    }
    else{
        return done();
    }

}

function processRep(rep, done) {
    const movieName = path.basename(rep)
    const source = rep + '/*.' + config.ext
    const files = glob.sync(source)

    return src(source)
        .pipe(spritesmith({
            imgName: movieName + '.' + sizeOf(files[0]).type,
            cssName: movieName + '.css',
            algorithm: 'left-right',
            algorithmOpts: {sort: false},
        })).img
        .on('error', handleErrors)
        .pipe(buffer())
        .pipe(dest(
            conf.getDestPath(path.join(conf.getFileBundleName(source), config.dest))
        ))
}

function createScssTemplate(reps) {
    const movies = reps.map(rep => {
        const movieName = path.basename(rep)
        const source = rep + '/*.' + config.ext
        const files = glob.sync(source)
        return {
            movieName: movieName,
            framesNumber: files.length,
            sizes: sizeOf(files[0]),
            version: Math.floor(Math.random()*10000)
        }
    })

    const template = config.css.templatePath === 'default' ? path.join(__dirname, 'template', '_template.scss') : config.css.templatePath

    const templateOptions = {
        movies: movies,
        config: config,
    };

    return src(template)
        .pipe(consolidate('lodash', templateOptions))
        .pipe(dest(path.join(conf.getFileBundlePath(reps[0]), config.css.dest)))
}

function onFileChanged(file, data) {
    Messenger.error('Il y a pas mal de dépendances sur la taches movies. Vous feriez mieux de lancer un "./gulp movies" en dehors du watch.');
}

/**
 * Lance les actions au watch des sass.
 */
function watchProcess() {
    const watcher = watch(conf.getSrcPath(path.join('*', config.watch + "." + config.ext)))

    watcher.on('change', (file, data) => onFileChanged(file, data))
    watcher.on('add', (file, data) => onFileChanged(file, data))
    watcher.on('unlink', (file, data) => onFileChanged(file, data))
}

exports.watchProcess = watchProcess
exports["movies"] = movies

/**
 * A décommenter si la tache doit êter exécutée avant le watch.
 */
//exports.beforeWatch = exports["movies"]
