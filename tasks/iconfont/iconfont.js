const {src, dest, watch} = require('gulp')
const path = require('path')
const iconfont = require('gulp-iconfont')
const consolidate = require('gulp-consolidate')
const clean = require('gulp-dest-clean')
const del = require('del')
const rimraf = require('rimraf')

const bundleTools = require('../../utils/BundleTools')
const messenger = require('../../utils/Messenger')
const Reload = require('../../utils/ReloadTools')
const conf = require('../../utils/Config')
const configIconfont = conf.get('iconfont').iconfont

/**
 * Génère les font liés aux icons.
 * @return {any}
 */
function generateIconfont(done) {
    return bundleTools(conf.getSrcPath('*'), done).src(processBundle)
}

/**
 *
 * @param source
 * @return {any}
 */
function processBundle(source) {
    // Récupération de la pattern des points d'entrée
    source = path.join(source, configIconfont.src + configIconfont.ext)
    const bundlePath = conf.getFileBundlePath(source);

    // Création d'un repertoire temporaire pour éviter de renommer des sources.
    const workRep = path.join(bundlePath, 'icons_tmp');

    return src(source)
        .pipe(clean(workRep))
        .pipe(dest(workRep))
        .pipe(iconfont(configIconfont.config))
        .on('glyphs', (glyphs, options) => generateCss(glyphs, options))
        .pipe(dest(path.join(bundlePath, configIconfont.dest)))
        .on('finish', () => {
            // Suppression du rep temporaire.
            rimraf.sync(workRep)
        })
}

/**
 * Génère le css associé au font icon.
 * @param glyphs
 * @param options
 */
function generateCss(glyphs, options) {
    const template = configIconfont.css.templatePath === 'default' ? path.join(__dirname, 'template', '_template.scss') : configIconfont.css.templatePath

    const templateOptions = {
        ...{
            glyphs: glyphs,
            cacheToken: Date.now(),
        },
        ...configIconfont.css
    };

    return src(template)
        .pipe(consolidate('lodash', templateOptions))
        .pipe(dest(path.join(conf.getFileBundlePath(glyphs[0].path), configIconfont.css.dest)))
}

function onFileChanged(file, data) {
    // Pour des raisons de perfs, on ne reconstruit que le bundle concerné par le fichier
    processBundle(conf.getFileBundlePath(file))
        // On indique au tool de reload automatique qu'un changement à eu lieu.
        .pipe(Reload.dispatchChange(file, data))
}

/**
 * Lance les actions au watch des js.
 */
function watchProcess() {
    const watcher = watch(conf.getSrcPath(path.join('*', configIconfont.watch + configIconfont.ext)))
    watcher.on('change', (file, data) => onFileChanged(file, data));
    watcher.on('add', (file, data) => onFileChanged(file, data));
}


exports.watchProcess = watchProcess
exports.iconfont = generateIconfont
