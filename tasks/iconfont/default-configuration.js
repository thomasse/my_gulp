exports.defaultConfiguration = {
    iconfont: {
        ext: 'svg',
        src: 'icons/**/*.',
        watch: 'icons/**/*.',
        dest: 'fonts/iconfont',
        config: {
            fontName: 'iconfont',
            fontHeight: 1001,
            normalize: true,
            prependUnicode: true,
            formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
            timestamp: Math.round(Date.now() / 1000),
        },
        css: {
            templatePath: 'default',
            fontName: 'iconfont',
            fontPath: './fonts/iconfont/',
            className: 'ic',
            dest: 'scss/iconfont/'
        }
    }
}
