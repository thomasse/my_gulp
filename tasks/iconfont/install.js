/**
 * à l'insatllation de la tache.
 * @param data
 */
module.exports.onInstall = (data) => {
	const path = require('path')
	const fs = require('fs');
	const glob = require('glob')
	const messenger = require('../../utils/Messenger')
	const dependency = require('../../utils/Dependency')

	messenger.info('Installation des patchs nécessaires');
	if (!fs.existsSync(path.join(data.conf.root, 'patches'))) {
		fs.mkdirSync(path.join(data.conf.root, 'patches'))
	}

	// Copy du patch.
	glob.sync(path.join(__dirname, 'patches', '*')).forEach(file => {
		fs.copyFileSync(file, path.join(data.conf.root, 'patches', file.split('/').slice(-1)[0]))
	})

	// Installation du patch.
	dependency.patchPackages('gulp-iconfont');

}
