/**
 * Permet de supprimer les uuid autogénéré par iconfont. Pas de meilleur moyen :/
 */
exports.cleanIconFont = function(){
    const conf = require('../../utils/Config')
    const configIconfont = conf.get('iconfont').iconfont
    const glob = require("glob")
    const fs = require('fs')
    const path = require('path')

    // Récupération du bundle en paramète si existant
    let bundle = process.argv.filter(item => item.indexOf('--')===0)[0];
    bundle = bundle ? bundle.replace('--','') : '*'

    if( configIconfont ){
        glob.sync(path.join(conf.getSrcPath(bundle), configIconfont.src + configIconfont.ext)).forEach( file => {
            const filenameData = path.basename(file).split('-');
            if( filenameData.length> 1 ){
                fs.renameSync(
                    file,
                    file.replace(path.basename(file), path.basename(file).split('-').slice(1).join('-'))
                )
            }
        });
    }
}
