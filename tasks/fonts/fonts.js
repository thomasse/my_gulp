const { src, dest, series } = require('gulp');
const path = require('path')

const BundleTools = require('../../utils/BundleTools')
const conf = require('../../utils/Config')
const configFonts = conf.get('fonts').fonts



/**
 * Parcours les fonts
 * @param done
 * @return {*}
 */
function fonts(done){
    return BundleTools(conf.getSrcPath(configFonts.src), done).src(processFontsInBundle)
}

/**
 * Effectue la tache de font dans un bundle précis.
 * @param bundlePath
 * @return {any}
 */
function processFontsInBundle (bundlePath) {
    const destination = conf.getDestPath(
        path.join(
            conf.getFileBundleName(bundlePath),
            configFonts.dest
        )
    )

    return src(bundlePath)
        .pipe(dest(destination));
}

exports.fonts = fonts;
