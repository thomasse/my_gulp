// Default conf
exports.defaultConfiguration = {
    'sprites': {
        // Répertoire des sources
        ext: '*',
        src: 'sprites/**/*',
        watch: 'sprites/**/*',
        // Répertoire de dstination.
        dest: './',
        sprites:{
            imgName: 'sprite.png',
            cssName: 'scss/base/_sprite.scss',
        }
    }
}
