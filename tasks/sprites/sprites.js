const {watch, src, dest} = require('gulp')
const path = require('path')
const spritesmith = require('gulp.spritesmith')
const merge = require('merge-stream')

const messenger = require('bim-gulp/utils/Messenger')
const Reload = require('bim-gulp/utils/ReloadTools')
const bundleTools = require('bim-gulp/utils/BundleTools')
const handleErrors = require('bim-gulp/utils/error')
const conf = require('bim-gulp/utils/Config')
const config = conf.get('sprites').sprites

/**
 * Créer des sprites
 */
function sprites(done) {
    bundleTools(conf.getSrcPath('*'), done).src(processBundle)
}

/**
 * Processus scopé dans un bundle particulier.
 */
function processBundle(source) {
    // Récupération de la pattern des points d'entrée
    source = path.join(source, config.src + '.' + config.ext)

    const bundleName = conf.getFileBundleName(source)

    // Définition du nom du fichier de sortie css.
    const imgFile = config.sprites.imgName ? config.sprites.imgName : 'sprite.png';
    const cssFile = config.sprites.cssName ? config.sprites.cssName : '_sprite.scss';
    const imgPath = config.sprites.imgPath ? config.sprites.imgPath : imgFile;

    // Définitino du sprite.
    const spriteData = src(source)
        .pipe(spritesmith({
            ...config.sprites,
            ...{
                imgPath: imgPath,
                imgName: path.basename(imgFile),
                cssName: path.basename(cssFile),
            }
        }))
        .on('error', handleErrors);

    const imgData = spriteData.img
        .pipe(dest(conf.getDestPath(path.join(bundleName, path.dirname(imgFile)))))

    const cssData = spriteData.css
        .pipe(dest(conf.getSrcPath(path.join(bundleName, path.dirname(cssFile)))))

    return merge(imgData, cssData)
}

/**
 * Lance les actions au watch des sass.
 */
function watchProcess() {
    const watcher = watch(conf.getSrcPath(path.join('*', config.watch + '.' + config.ext)))
    watcher.on('add', (file, data) => {
        // Pour des raisons de perfs, on ne reconstruit que le bundle concerné par le fichier
        processBundle(conf.getFileBundlePath(file))
            // On indique au tool de reload automatique qu'un changement à eu lieu.
            .pipe(Reload.dispatchChange(file, data))
    })
}

exports.watchProcess = watchProcess
exports["sprites"] = sprites

/**
 * A décommenter si la tache doit êter exécutée avant le watch.
 */
//exports.beforeWatch = exports["sprites"]
