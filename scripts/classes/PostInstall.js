const fs = require('fs')
const path = require('path')
const messenger = require('../../utils/Messenger.js')
const taskLoader = require('../../utils/TaskLoader')

class PostInstallClass {

    constructor() {
        this.root = process.cwd().split('node_modules')[0]

        // Load les taches disponibles
        taskLoader.loadDefaultConfiguration(taskLoader.defaultTasksDir)
    }

    /**
     * Lance le script post install.
     */
    run() {
        messenger.title('Installation du entrypoint gulp.')
        this.initGulpScript()
    }


    /**
     * Initialise le lien symbolique vers le binaire de gulp.
     */
    initGulpScript() {
        const dest = path.join(this.root, 'gulp')

        if (!fs.existsSync(dest)) {
            messenger.message('Create : ' + dest)
            fs.writeFileSync(dest, 'node node_modules/bim-gulp/scripts/stack-install.js')
            fs.chmodSync(dest, '775')
        }
    }
}

module.exports = new PostInstallClass()
