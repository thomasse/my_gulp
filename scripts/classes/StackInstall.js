const fs = require('fs')
const path = require('path')
const glob = require('glob')
const {src, series, dest} = require('gulp')
const messenger = require('../../utils/Messenger.js')
const conf = require('../../utils/Config')
const taskLoader = require('../../utils/TaskLoader')
const consolidate = require('gulp-consolidate')
const prompts = require('prompts')
const createBundle = require('../../mandatory_tasks/create-bundle/create-bundle');

class PostInstallClass {

    constructor() {
        this.root = process.cwd().split('node_modules')[0]

        // Load les taches disponibles
        taskLoader.loadDefaultConfiguration(taskLoader.defaultTasksDir)
    }

    /**
     * Lance le script post install.
     */
    run() {
        messenger.title('Installation des fichiers de configuration gulp.')
        this.initGulpScript()


        if (!fs.existsSync(path.join(this.root, 'gulpfile.js'))) {
            // Prompt
            prompts([{
                type: 'select',
                name: 'stack',
                message: 'Quelle stack voulez-vous initialiser ?',
                choices: [
                    {'value': 'default', title: 'Stack par défaut'},
                    {'value': 'custom', title: 'Stack personnalisée'},
                ]
            }])
                .then(result => this.onStackChoice(result))
        }
    }

    /**
     * Permet de choisir les tâches à utiliser.
     * @param result
     */
    onStackChoice(result) {
        if (result.stack === 'default') {
            this.onAnswer(result)
        } else {
            prompts([{
                type: 'multiselect',
                name: 'tasks',
                message: 'Quelles sont les tâches que vous voulez utiliser ?',
                choices: Object.keys(conf.config).map(task => {
                    return {value: task, title: task}
                })
            }]).then(onlyTasks => this.onAnswer({...result, ...{onlyTasks: onlyTasks.tasks}}))
        }
    }

    onAnswer(answers) {
        this.answers = answers

        // On load la conf uniquement nécessaire en fonction des taches choisies.
        if (this.answers.stack === 'custom') {
            conf.config = {};
            taskLoader.loadDefaultConfiguration(taskLoader.defaultTasksDir, this.answers.onlyTasks)
            taskLoader.loadAllTasks(taskLoader.defaultTasksDir, [], this.answers.onlyTasks, true)
        } else {
            taskLoader.loadDefaultConfiguration(taskLoader.defaultTasksDir)
            taskLoader.loadAllTasks(taskLoader.defaultTasksDir, [], [], true)
        }

        this.copyFiles()

    }

    /**
     * Copie les fichier de configuration.
     */
    copyFiles() {
        this.filesCopiedCount = 0
        this.filesToCopy = glob.sync(path.join(__dirname, '../', 'templates/*'));
        this.filesToCopy.forEach(file => {
            const fileName = file.split('/').slice(-1)[0]
            const destination = path.join(this.root, fileName)

            if (!fs.existsSync(dest)) {
                messenger.message('Copy : ' + destination)
                if (!fs.statSync(file).isDirectory()) {
                    const customCopyMethod = this.getCustomCopyMethod(fileName)
                    customCopyMethod.call(this, file, destination);
                }
            }
        });
    }

    onFileCopied() {
        if (++this.filesCopiedCount === this.filesToCopy.length) {
            createBundle(() => {
                // Launch gulp cli
                require('gulp-cli')();
            })
        }
    }

    /**
     * Initialise le lien symbolique vers le binaire de gulp.
     */
    initGulpScript() {
        const dest = path.join(this.root, 'gulp')

        if (fs.existsSync(dest)) {
            messenger.message('Delete : ' + dest)
            fs.unlinkSync(dest);
        }

        messenger.message('Create : ' + dest)
        fs.symlinkSync('node_modules/gulp/bin/gulp.js', dest)
        fs.chmodSync(dest, '775')
    }

    /**
     * Retourne la méthode de custom de copie de fichier si existante.
     */
    getCustomCopyMethod(filename) {
        filename = filename.split('.')[0]
        const methodName = ['customCopy', filename[0].toUpperCase(), filename.slice(1)].join('');
        const method = typeof (this[methodName]) === 'function' ? this[methodName] : this.defaultCopy;
        return method;
    }

    /**
     * Copy par défaut.
     * @param file
     * @param dest
     */
    defaultCopy(file, dest) {
        fs.copyFileSync(file, dest)
        this.onFileCopied()
    }

    /**
     * Copy le fichier d'override de conf.
     * @param file
     * @param dest
     */
    customCopyConfig(file, destination) {
        // Récupération de la conf.
        const templateOptions = {
            config: conf.config
        }

        if (fs.existsSync(path.join(this.root, 'config.js'))) {
            return;
        }

        // on utilise consolidate comme moteur de template.
        // Donc ici en fait on lance une tache gulp.
        return series((cb) => {
            return src(file)
                .pipe(consolidate('lodash', templateOptions))
                .pipe(dest(this.root))
        })(() => this.onFileCopied())
    }

    /**
     * Copy du gulpfuile.
     *
     * @param file
     * @param destination
     * @return {any}
     */
    customCopyGulpfile(file, destination) {
        // Récupération de la conf.
        const templateOptions = {
            answers: this.answers
        }

        // on utilise consolidate comme moteur de template.
        // Donc ici en fait on lance une tache gulp.
        return series((cb) => {
            return src(file)
                .pipe(consolidate('lodash', templateOptions))
                .pipe(dest(this.root))
        })(() => this.onFileCopied())
    }
}

module.exports = new PostInstallClass()
