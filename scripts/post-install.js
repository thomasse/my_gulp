/**
 * Déplacement des templates dans le répertoire root du projet.
 */

const PostInstall = require('./classes/PostInstall.js')
PostInstall.run();
