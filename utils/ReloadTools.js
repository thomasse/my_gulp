const conf = require('./Config')
const Messenger = require('./Messenger')
const fs = require('fs')
const path = require('path')
const prompts = require('prompts')
const glob = require('glob')
const dependency = require('./Dependency')

class ReloadToolsClass {

    constructor() {
        this.localPath = path.join(conf.root, 'local.config.js');
    }

    /**
     * INitialisation du reload tool.
     */
    initReloadTool() {
        const promise = new Promise((resolve)=>{
            this.resolve = resolve
        });

        if (!fs.existsSync(this.localPath)) {
            this.promptChoice()
        } else {
            this.startReloadTool()
            // resolve.
            setTimeout( ()=> {this.resolve()}, 1)
        }

        return promise
    }

    /**
     * Demande à l'utilsateur l'outil de reload à utiliser.
     */
    promptChoice() {
        // Prompt
        prompts([
            {
                type: 'select',
                name: 'type',
                message: 'Quel outils de reload voulez-vous utiliser ?',
                choices: this.getAvailableTools(),
            }
        ]).then(result => this.installTool(result.type))
    }

    /**
     * Liste l'ensemble des outils permettant un live reload.
     */
    getAvailableTools() {
        return glob.sync(path.join(__dirname, './reload/*.js')).map(file => {
            const data = require(file)
            return {
                value: data.name,
                title: data.description,
            }
        });
    }

    /**
     * Install un pacakge
     * @param packageName
     */
    installTool(packageName) {
        dependency.install(
            [packageName],
            {saveDev: true, save: false},
            () => this.initLocal(packageName))
    }

    /**
     * Initialise le fichier de conf.
     * @param type
     */
    initLocal(type) {
        const ReloadToolClass = require('./reload/' + type).reloadTool;
        this.reloadTool = this.reloadTool || new ReloadToolClass({});

        if( 'function' === typeof(this.reloadTool.install) ){
            const data = this.reloadTool.install();

            if( !!data && typeof data.then === 'function'){
                data.then(() => this.afterInstall(type))
            }
            else{
                this.afterInstall(type)
            }
        }
        else{
            this.afterInstall(type)
        }
    }

    /**
     * Action after install.
     * @param type
     */
    afterInstall(type){
        const value = {
            type: type,
            config: this.getDefaultConfigByType(type)
        }

        fs.writeFileSync(this.localPath, "module.exports = " + JSON.stringify(value))
        Messenger.info(type + ' a été installé.  \nVeuillez vérifier la configuration dans ' + this.localPath);
        this.startReloadTool()

        this.resolve()
    }

    /**
     * Renvoie la conf par défaut par type.
     */
    getDefaultConfigByType(type) {
        const ReloadToolClass = require('./reload/' + type).reloadTool;
        this.reloadTool = this.reloadTool || new ReloadToolClass({});
        return this.reloadTool.getDefaultConfig();
    }

    /**
     * Lance l'outl de reload.
     */
    startReloadTool() {
        this.localConfig = require(this.localPath);

        const ReloadToolClass = require('./reload/' + this.localConfig.type).reloadTool;
        this.reloadTool = new ReloadToolClass(this.localConfig.config);
        this.reloadTool.start();
    }

    /**
     * Lance une action au changement de fichier.
     * @param file
     * @param data
     * @return {*}
     */
    dispatchChange(file, data) {
        if (this.reloadTool) {
            return this.reloadTool.dispatchChange(file, data)
        }
    }

    forceRefresh(file, data) {
        if (this.reloadTool) {
            return this.reloadTool.forceRefresh(file, data)
        }
    }
}

module.exports = new ReloadToolsClass()
