const glob = require('glob')
const {parallel} = require('gulp')
const path = require('path')

class BundleTools {

    constructor(globPaths, done) {
        this.done = done
        this.globPath = typeof globPaths === 'string' ? [globPaths]: globPaths
        this.conf = require('./Config')
    }

    /**
     * Permet de lancer plusieurs pipe scopés par bundle
     * @param callback
     */
    src(callback) {
        // Définition de la liste de bundles à parcourir.
        this.bundleList = this._getBundleRegExFromGlob(this.globPath);

        // On définit la liste
        const callbackList = []
        Object.keys(this.bundleList).forEach(bundlePath => {
            callbackList.push(() => {
                return callback(bundlePath, this.done)
            });
        })

        if( callbackList.length ){
            return parallel(...callbackList)(this.done)
        }

        this.done();
    }


    /**
     * @private
     */
    _getBundleRegExFromGlob(globPaths){
        const bundleList = {};
        globPaths.forEach(globPath => {
            try {
                // Définition du suffix de selection dans le scope du bundle.
                let suffix = globPath.split(this.conf.config.build.src)[1].split('/')
                if( suffix[0] === '*' ){
                    suffix= suffix.slice(1)
                }
                suffix = suffix.join('/')

                glob.sync(globPath).forEach(filePath => {
                    // on renseigne la regex dans le scope du bundle.
                    let bundleRegEx = path.join(this.conf.getFileBundlePath(filePath),suffix)
                    bundleList[bundleRegEx] = false
                })
            } catch (e) {
                console.log(e);
                process.exit();
            }
        })

        return bundleList;
    }
}

// Export un objet BundleTools
module.exports = (src, done) => { return new BundleTools(src, done) }
