const path = require('path')
const fs = require('fs')

class ConfigClass {

  constructor() {
    this.root = process.cwd()
    this.prod = true
    this.enabledTasks = [];
  }

  /**
   * Retourne le type de build.
   * @return {boolean}
   */
  isProd() {
    return this.prod
  }

  /**
   * Active le mode prod
   */
  setProdMode() {
    this.prod = true
  }

  /**
   * Désactive le mode prod.
   */
  setDevMode() {
    this.prod = false
  }

  /**
   * Retourne les configurations.
   * @param params
   * @return {{}}
   */
  get(...params) {
    const conf = {}

    this._loadConfiguration(params.filter(item => !Object.keys(this.config).includes(item)))

    params.forEach(key => {
      if (this.config[key]) {
        conf[key] = this.config[key]
      }
    })

    return conf
  }

  /**
   * Load les configuration
   * @param params
   * @private
   */
  _loadConfiguration(confNames) {
    this.taskLoader = this.taskLoader || require('./TaskLoader')
    this.taskLoader.loadSpecificConfiguration(confNames);
  }

  /**
   * Update conf
   */
  addConfiguration(defaultConfig) {
    const overrideFilePath = path.join(this.root, 'config.js')
    const override = fs.existsSync(overrideFilePath) ? require(overrideFilePath) : {}
    // On override
    this.config = {
      ...this.config,
      ...defaultConfig,
      ...override
    }
  }

  /**
   * Initialise le mode de build dev|prod
   */
  initMode() {
    // Définition du mode
    if (process.argv.includes('--production')) {
      this.setProdMode()
    } else {
      this.setDevMode()
    }
  }

  /**
   * Retourne le chemin relatifs des sources.
   * @param src
   * @return {string}
   */
  getSrcPath(src = '') {
    return path.join('./', this.config.build.src, src);
  }

  /**
   * Retourne le chemin relatifs des destinations.
   * @param src
   */
  getDestPath(src = '') {
    return path.join(
      this.isProd() ? this.config.build.prod : this.config.build.dev,
      src
    )
  }

  /**
   * Renvoie le chemin du bundle auquel appartient le fichier courant.
   * @param filePath
   * @return {string}
   */
  getFileBundlePath(filePath) {
    // Récupération du bundle nom du bundle.
    const src = this.config.build.src;
    return path.join(this.root, src, this.getFileBundleName(filePath))
  }

  /**
   * Retourne le nom du bundle.
   * @param filePath
   * @return {string}
   */
  getFileBundleName(filePath) {
    const bundleRootPath = path.join(this.root, this.config.build.src)
    const fileRootPath = path.join(this.root, filePath)
    return fileRootPath.split(bundleRootPath)[1].split('/')[0]
  }

  enableTasks(files) {
    files
      .map(file => file.split('/').slice(-2)[0])
      .map(task => {
        if (this.enabledTasks.indexOf(task) < 0) {
          this.enabledTasks.push(task);
        }
      })
  }

  getEnabledTasks() {
    return this.enabledTasks;
  }
}

module.exports = new ConfigClass()
