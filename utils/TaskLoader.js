const glob = require('glob')
const path = require('path')
const conf = require('./Config')
const messenger = require('./Messenger')
const dependency = require('./Dependency')
const installer = require('./Installer')

class TaskLoaderClass {

  constructor() {
    // Initialisation du mode.
    conf.initMode()

    this.tasks = []
    this.watchProcesses = []
    this.beforeWatch = {}
    this.directories = []

    const root = __dirname.split('node_modules')[0]
    this.mandatoryDir = path.resolve(__dirname + '/../mandatory_tasks')
    this.defaultTasksDir = path.resolve(__dirname + '/../tasks')
  }

  /**
   * Load les tasks par défaut.
   */
  loadDefaultTasks(exports, onlyTasks = []) {
    // On load les taches mandatory.
    this.loadTasks(this.mandatoryDir, exports)
    // On load toutes les tasks.
    this.loadTasks(this.defaultTasksDir, exports, onlyTasks, true)
  }

  /**
   * Load les tasks présentes dans le répertoire dir
   *
   * @param dir
   */
  loadTasks(dir, exports, onlyTasks = [], isActionsTasks = false) {
    messenger.title('Chargement des tâches de ' + dir);
    messenger.info('Lancer `./gulp help` pour lister l\'ensemble des tâches disponibles ')

    // On stocke le repertoire.
    if (!this.directories.includes(dir)) {
      this.directories.push(dir)
    }

    this.loadDefaultConfiguration(dir, onlyTasks);
    this.loadAllTasks(dir, exports, onlyTasks, isActionsTasks)
  }

  /**
   * Load des configuration liée aux taches.
   * @param dir
   * @param exports
   */
  loadDefaultConfiguration(dir, onlyTasks) {
    let configuration = {};

    // Parcours de la config par défaut.
    glob.sync(path.join(__dirname, 'config', '*.js')).forEach(file => {
      configuration = {...configuration, ...this.loadConfiguration(file)}
    })

    // // Parcours de chaque fichier de tache.
    glob.sync(path.join(this.getBaseSrcPath(dir, onlyTasks), 'default-configuration.js')).forEach(file => {
      dependency.load(() => {
        configuration = {...configuration, ...this.loadConfiguration(file)}
      })
    })

    // Ajout de la configuration;
    conf.addConfiguration(configuration)
  }

  /**
   * Load un fichier de configuration.
   * @param file
   * @param configuration
   */
  loadConfiguration(file) {
    const data = require(file);
    return data.defaultConfiguration || {}
  }

  /**
   * Load toutes les taches.
   * @param dir
   * @param exports
   */
  loadAllTasks(dir, exports, onlyTasks, isActionsTasks = false) {

    // Récupération des fichiers liés aux taches.
    const tasksFiles = glob.sync(path.join(this.getBaseSrcPath(dir, onlyTasks), '*.js'));

    if (isActionsTasks){
       conf.enableTasks(tasksFiles);
    }

    // On place les install en début de process.
    tasksFiles.sort((a, b) => {
      if (a.indexOf('/install.js') > -1) {
        return -1
      }
      if (b.indexOf('/install.js') > -1) {
        return 1
      }
      return 0
    })

    // Parcours de chaque fichier de tache.
    tasksFiles.forEach(file => {
      dependency.load(() => this.loadOneTask(exports, file))
    })
  }

  /**
   * Load une tache.
   * @param file
   */
  loadOneTask(exports, file) {
    const taskData = require(file)

    // INstallation.
    this.installTask(file, taskData);

    // Pour chaque tache récupérée, on affecte l'action adéquate.
    Object.keys(taskData).forEach(taskName => {
      switch (taskName) {
        case 'watchProcess':
          // On store les tasks watchProcess dans un tableau séparé.
          this.watchProcesses.push(taskData[taskName])
          break;
        case 'beforeWatch':
          this.beforeWatch.taskName = taskData[taskName]
          break;
        case 'defaultConfiguration' :
        case 'install':
          // Specifique. Ne rien faire.
          break;
        default:
          exports[taskName] = taskData[taskName]
          this.tasks[taskName] = file
          break;
      }
    })
  }

  /**
   * Install une tache si nécessaire.
   * @param file
   */
  installTask(file, taskData) {
    if (!installer.installed(file)) {
      if (taskData.onInstall) {
        taskData.onInstall(
          {
            'taskLoader': this,
            'conf': conf,
            'dependency': dependency
          }
        );
      }
      installer.onInstalled(file)
    }
  }

  /**
   * Renvoie la liste des taches disponibles
   */
  getTasksList() {
    return this.tasks;
  }

  /**
   * REtourne le chemin de recherche  des sources de base.
   * @param dir
   * @param onlyTasks
   */
  getBaseSrcPath(dir, onlyTasks = []) {
    let taskRep = '*'
    switch (onlyTasks.length) {
      case 0:
        taskRep = '*'
        break
      case 1:
        taskRep = onlyTasks[0]
        break
      default:
        taskRep = '{' + onlyTasks.join(',') + '}'
    }

    if (dir.charAt(0) === '/') {
      return path.join(dir, taskRep)
    } else {
      return path.join(conf.root, dir, taskRep)
    }
  }

  /**
   *
   */
  loadSpecificConfiguration(confNames) {
    this.directories.forEach(dir => this.loadDefaultConfiguration(dir, confNames))
  }
}

module.exports = new TaskLoaderClass()
