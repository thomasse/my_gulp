class GulpLivereload{

    constructor(config) {
        this.config = config;
    }

    /**
     * Start reload tools
     */
    start(){
        this.livereload = require('gulp-livereload');
        this.livereload({ start: true })
        this.livereload.listen();
    }

    /**
     * Action on change.
     * @param path
     */
    dispatchChange(path, data){
        return this.livereload();
    }

    /**
     * Force le reload de la page entière.
     * @param path
     * @param data
     */
    forceRefresh(path, data){
        return this.livereload.reload('/');
    }

    /**
     * Retourne la configuration par défaut insérée dans local.config.js
     */
    getDefaultConfig(){
        return {}
    }
}

module.exports.name = "gulp-livereload"
module.exports.description = "Livereload : Utilise une extension chrome pour identifier les changements (https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=fr)"
module.exports.reloadTool = GulpLivereload;
