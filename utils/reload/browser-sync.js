class BrowserSync {

	constructor(config) {
		this.config = config
	}

	/**
	 * Start reload tools
	 */
	start() {
		this.browserSync = require('browser-sync').create();
		this.browserSync.init(this.config)
	}

	/**
	 * Action on change.
	 * @param path
	 */
	dispatchChange(path, data) {
		return this.browserSync.stream()
	}

	/**
	 * Force le reload de la page entière.
	 * @param path
	 * @param data
	 */
	forceRefresh(path, data) {
		return this.browserSync.reload()
	}

	/**
	 * Retourne la configuration par défaut insérée dans local.config.js
	 */
	getDefaultConfig() {
		return this.installConfig || {
			proxy: "@todo: renseigner le proxy",
		}
	}

	install() {
		const prompts = require('prompts');
		this.installPromise = new Promise((resolve, reject) => {
			this.installResolve = resolve;
		})
		prompts([
			{
				type: 'text',
				name: 'proxy',
				message: 'Un proxy ? ',
			}
		]).then(result => {
			this.installConfig = result
			this.installResolve()
		})

		return this.installPromise
	}
}

module.exports.name = "browser-sync"
module.exports.description = "Browser-sync : Lance un serveur automatiquement pour identifier les rafraichissements (warning sur les environnements docker)"
module.exports.reloadTool = BrowserSync;
