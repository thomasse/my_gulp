const fs = require('fs')
const conf = require('./Config')
const path = require('path')
const messenger = require('./Messenger')

class InstallerToolsClass {

	constructor() {
		// Cache des installations de taches.
		this.dir = path.join(conf.root, 'node_modules', '.cache');
		this.file = path.join(this.dir, 'tasks.json')
		if (!fs.existsSync(this.file)) {
			this.data = {'installed': []};
			this.save();
		}
		else{
			this.data = require(this.file)
		}
	}

	/**
	 * Renvoi vrai si la tache a déjà été installée.
	 * @param file
	 * @return {boolean}
	 */
	installed(file) {
		return this.data.installed.includes(file)
	}

	/**
	 * After install
	 * @param file
	 */
	onInstalled(file){
		messenger.info(file + ' installé ');
		this.data.installed.push(file)
		this.save()
	}

	save() {
		if (!fs.existsSync(this.file)) {
			fs.mkdirSync(this.dir, { recursive: true })
		}
		fs.writeFileSync(this.file, JSON.stringify(this.data))
	}
}

module.exports = new InstallerToolsClass()
