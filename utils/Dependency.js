const Messenger = require('./Messenger')
const conf = require('./Config')
const cp = require('child_process')
const fs = require('fs')
const path = require('path')
const glob = require('glob')

class DependencyClass {

    constructor() {
        // Stocke les packages en erreur.
        this.packagesToInstall = []
        this.defaultOptions = {
            save: true,
            output: true,
        }

        this.packageErrors = [];
    }

    /**
     * Install un package.
     * @param packages
     */
    install(packages, options, cb) {
        packages = Array.isArray(packages) ? packages : [packages]

        options = {...this.defaultOptions, ...options}

        let cmdString = this.isYarn() ? "yarn add " : "npm install "
        cmdString += packages.join(" ") + " "
            + (options.save ? " --save" : " --no-save")
            + (options.saveDev ? " --save-dev" : "")
            + (options.legacyBundling ? " --legacy-bundling" : "")
            + (options.noOptional ? " --no-optional" : "")
            + (options.ignoreScripts ? " --ignore-scripts" : "");

        Messenger.info('Install packages : ' + packages.join())
        cp.execSync(cmdString, {cwd: conf.root})

        // On lance la suite.
        cb();
    }

    isYarn(){
        return fs.existsSync(path.join(conf.root,'yarn.lock'))
    }

    /**
     * Patch packages
     */
    patchPackages(packagesToPatch) {
        packagesToPatch = Array.isArray(packagesToPatch) ? packagesToPatch : [packagesToPatch]
        this.load((data) => {
            // On install d'abord le pacakge sans le require.
            this.install(packagesToPatch, {save: true}, () => {
                const binPath = './node_modules/.bin/patch-package'
                if (fs.existsSync(binPath)) {
                    // Gestion des patches.
                    cp.execSync(binPath, {cwd: __dirname.split('node_modules/')[0]})
                } else {
                    // Installation de patch packages.
                    const patchPackages = require('patch-package');
                    // Une fois installé on repassera dans la première condition.
                }
            })
        })

    }

    /**
     * Tente de jouer la callback, et stock les modules manquants en cas de MODUEL_NOT_FOUND
     * @param cb
     */
    load(cb, errorData = {}) {
        try {
            // Execute l'action
            cb(errorData)
        } catch (e) {
            // On stocke le module pas trouvé.
            if (e.code === 'MODULE_NOT_FOUND') {
                const packageToInstall = e.message.split("'")[1]
                if( this.packageErrors.indexOf(packageToInstall) < 0 ){
                    Messenger.error('Le module ' + packageToInstall + ' n\'a pas été trouvé. On l\'installe dynamiquement')
                    this.packageErrors.push(packageToInstall)
                    this.install(packageToInstall, {save: true}, () => this.load(cb, {
                        'package': packageToInstall
                    }))
                }
            } else {
                throw e
            }
        }
    }

}

module.exports = new DependencyClass()
