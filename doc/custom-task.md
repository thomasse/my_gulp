# Créer une tache custom 

## Commande
Vous pouvez créer une tâche via la ligne de commande suivante : 
```./gulp create-task```

Ensuite vous allez devoir renseigner les éléments suivants :
- Le nom de la tache (qui sera camelisé pour l'appeler)
- La description
- Le répertoire où sera ajouter la tâche.

## Détail
### La configuration par défaut
### Le fichier de tache
### Le fichier d'installation
