# Le principe
 
Le workflow a pour but de répondre à plusieurs objectifs : 
1. [**Commun**](##commun)  
Travailler sur un workflow de build d'assets commun, utilisable sur différentes typologies de projet
1. [**Evolutif**](##evolutif)  
Faire évoluer le workflow rapidement et diffuser les évolutions simplement
1. [**Facile**](##facile)
Permettre de mettre en place rapidement et simplement un environnement de travail
1. [**Adéquat**](##adequat)  
Permettre de n'utiliser que le nécessaire pour alléger le traitement
1. [**Personnalisable**](##personnalisable)
Permettre de créer / customiser le workflow au besoin

## Commun
L'objectif d'avoir un workflow commun est de faciliter la prise en main des projets pour les différents
intervenants. Lorsqu'un collaborateur travail doit intervenir sur un nouveau projet, il doit déjà prendre 
en main un nouveau projet. Utiliser le même workflow sur les différents projets permet au contributeur de 
limiter le temps d'appréhension de nouveaux outils.


## Evolutif
@todo


## Facile
@todo


## Adéquat
@todo


## Personnalisable
@todo

